# problema

Al momento de terminar un proyecto para un cliente, debo subir el programa ya terminado y el respectivo código fuente. PeoplePerHour no permite la subida de programas ejecutables, tampoco permite la subida de archivos más grandes que 20 MiB. Por ello subo los archivos a Rackspace Cloud Files, sin embargo, el trabajo es algo pesado porque me gusta entregar enlaces "limpios" (CNAME).

# Solucion

Crear una aplicación en Django donde puedo subir los archivos que necesito subir y maneje de forma automática los procedimientos que usualmente hago de forma manual.
