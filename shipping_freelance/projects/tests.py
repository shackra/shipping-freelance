from django.test import TestCase
from .models import Project
from files.models import File

from os.path import join
from django.core.files.uploadedfile import SimpleUploadedFile


class ProjectTestCase(TestCase):

    def setUp(self):
        self.project1 = Project(
            name="Test Project", client_name="Jhonny", client_lastname="Test")
        self.file1 = File(
            file=SimpleUploadedFile(
                'project.exe',
                '01010100111010101'),
            upload_to=join(
                self.project1.client_name + " " + self.client_lastname,
                self.project1.name),
            source_code_file=SimpleUploadedFile(
                'project-sourcecode.zip',
                '01010100111010101'))

        self.file2 = File(
            file=SimpleUploadedFile(
                'project-2.exe',
                '01010100111010101'),
            upload_to=join(
                self.project1.client_name + " " + self.client_lastname,
                self.project1.name),
            source_code_file=SimpleUploadedFile(
                'project-2-sourcecode.zip',
                '01010100111010101'))

        self.file3 = File(
            file=SimpleUploadedFile(
                'project-3.exe',
                '01010100111010101'),
            upload_to=join(
                self.project1.client_name + " " + self.client_lastname,
                self.project1.name),
            source_code_file=SimpleUploadedFile(
                'project-3-sourcecode.zip',
                '01010100111010101'))
